import React from "react";
import { Link } from "react-router-dom";

function PageNotFound() {
  return (
    <div>
      We cannot find a page you were looking for.
      <Link to="/">Try the Homepage.</Link>
    </div>
  );
}

export default PageNotFound;
